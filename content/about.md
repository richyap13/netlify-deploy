+++
title = "About"
date = "2019-10-17"
aliases = ["about-us","about-hugo","contact"]
[ author ]
  name = "Hugo Authors"
+++

Hi 🌻 I’m Rich.  

I recently graduated with a degree in Computer Science at the University of the Philippines Diliman. These days, I’ve been writing articles on life and tech and self-studying Data Science through books and videos.